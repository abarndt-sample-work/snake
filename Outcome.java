//Author: Andrew Barndt
//Description: A java enum listing all possible outcomes for the
//game of snake.

public enum Outcome {
    CONTINUE,
    HIT_WALL,
    OFF_EDGE,
    HIT_SELF,
    FOOD_EATEN;
}