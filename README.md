**Overview**
<p>The code in this repository is used for the game of snake.</p>

**Gampeplay**
<p>The basic idea of the game is to move the snake around with the arrow keys to eat the food and the snake grows as more food is eaten. The snake must avoid hitting itself, running off the edge, or hitting walls.</p>

**Running the Program**
<p>Since the program uses several images and text files to run, it's easiest to download and run the .jar file in the repository to play the game. By default, it starts with a certain wall design, which can be changed by selecting a different map from the menu in the top left corner. There are also a couple easter eggs that require command line arguments to be set.</p>