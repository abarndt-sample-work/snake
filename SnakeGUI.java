//Author: Andrew Barndt
//Description: A java program that displays a GUI for the snake game. Uses the
//GameManager class to keep the back-end bookkeeping.

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class SnakeGUI {

    //Bookkeeping and display variables
    private String nameOfMap;
    private int squareSize = 20, gamesPlayed, timeCount;
    private GameManager manager;
    private JFrame frame;
    private JPanel gameWindow;
    private JLabel score;
    private Snake.Direction direction;
    private JButton startPause = new JButton("Start");
    private boolean isGameOver = false;

    //Easter egg variables
    private static boolean wantYankees, wantFire, wantHarambe, wantMinecraft;

    //Image variables and list to hold them
    private Image wallImage, currentFoodImage, snakeBody, harambe;
    private Image snakeHeadUp, snakeHeadDown, snakeHeadRight, snakeHeadLeft;
    private ArrayList<Image> images = new ArrayList<>();

    //Timer and random number generator
    private Timer timer;
    private Random random = new Random();


    /**
     * Sets up the images for the game display.
     */
    private void imageProcessing() {
        //Clears image list for new game; needed if square size will be
        //different
        images.clear();
        //If its easter egg is activated, wall is fire rather than brick
        if (wantFire) {
            wallImage = initializeImage("fire.png");
            wallImage = imageScaling(wallImage);
        }
        else if (wantMinecraft) {
            wallImage = initializeImage("grass.jpeg");
            wallImage = imageScaling(wallImage);
        }
        else {
            wallImage = initializeImage("wallimage.jpg");
            wallImage = imageScaling(wallImage);
        }
        //Create and size the food images
        if (wantMinecraft) {
            Image diamondFood = initializeImage("diamond.PNG");
            diamondFood = imageScaling(diamondFood);
            images.add(diamondFood);
            currentFoodImage = diamondFood;
        } else {
            Image bananaIcon = initializeImage("banana.jpg");
            bananaIcon = imageScaling(bananaIcon);
            Image strawberryIcon = initializeImage("strawberry.jpg");
            strawberryIcon = imageScaling(strawberryIcon);
            Image burgerIcon = initializeImage("burger.jpg");
            burgerIcon = imageScaling(burgerIcon);
            Image appleIcon = initializeImage("apple.jpg");
            appleIcon = imageScaling(appleIcon);
            images.add(bananaIcon);
            images.add(strawberryIcon);
            images.add(burgerIcon);
            images.add(appleIcon);
            currentFoodImage = images.get(random.nextInt(images.size()));
        }

        //Snake body is the Yankees logo if that easter egg is activated
        if (wantYankees) {
            snakeBody = initializeImage("yankees.jpg");
            snakeHeadLeft = initializeImage("yankees.jpg");
            snakeHeadRight = initializeImage("yankees.jpg");
            snakeHeadDown = initializeImage("yankees.jpg");
            snakeHeadUp = initializeImage("yankees.jpg");
        } else if (wantMinecraft) {
            snakeBody = initializeImage("chestplate.PNG");
            snakeHeadUp = initializeImage("steve.jpg");
            snakeHeadDown = snakeHeadUp;
            snakeHeadRight = snakeHeadUp;
            snakeHeadLeft = snakeHeadUp;
        }
        else {
            snakeBody = initializeImage("snakebody.PNG");
            snakeHeadUp = initializeImage("snakeheadup.PNG");
            snakeHeadDown = initializeImage("snakeheaddown.PNG");
            snakeHeadRight = initializeImage("snakeheadright.PNG");
            snakeHeadLeft = initializeImage("snakeheadleft.PNG");
        }


        snakeBody = imageScaling(snakeBody);
        snakeHeadUp = imageScaling(snakeHeadUp);
        snakeHeadDown = imageScaling(snakeHeadDown);
        snakeHeadRight = imageScaling(snakeHeadRight);
        snakeHeadLeft = imageScaling(snakeHeadLeft);

        //Only initialize the Harambe image if the command line said to
        if (wantHarambe) {
            harambe = initializeImage("harambe.jpg");
        }
    }

    /**
     * Sets up the image from the file. Returns the image or, if there's an
     * error that I've yet to see as long as the image is there, returns null.
     * @param name the name (ending in .jpg or .png or similar file) of the
     *             image
     * @return the image as read by ImageIO, or null otherwise
     */
    private Image initializeImage(String name) {
        try {
            return ImageIO.read(getClass().getClassLoader().getResource(name));
        } catch (IOException ie) {
            System.out.println("Error loading " + name);
            return null;
        }
    }

    /**
     * Resizes the image to fit into one square in the game.
     * @param img the image being resized
     * @return the resized image
     */
    private Image imageScaling(Image img) {
        return img.getScaledInstance(squareSize,squareSize,Image.SCALE_FAST);
    }

    /**
     * Nested class for the panel that displays the game. Also implements
     * ActionListener to listen for keyboard input.
     */
    private class GamePanel extends JPanel implements ActionListener {
        //Constructor sets up images, sets the panel's color and dimensions,
        //and starts the timer
        private GamePanel() {
            imageProcessing();
            this.setBackground(Color.BLACK);
            this.setPreferredSize(new Dimension(manager.getWidth() *
                    squareSize, manager.getHeight() * squareSize));
            timer = new Timer(250, this);
        }

        /**
         * ActionListener implementation. If the Harambe easter egg has been
         * activated, the panel is covered by the image of Harambe for a short
         * time; otherwise, it just updates the panel as normal.
         * @param e the event; in this case, the timer firing
         */
        public void actionPerformed(ActionEvent e) {
            if(nameOfMap.equals("maze-cross.txt") && wantHarambe &&
                    manager.getFoodEaten() == 2) {
                timeCount++;
                if(timeCount < 15) {
                    this.repaint();
                    return;
                }
            }
            updateGame();
            this.repaint();
        }

        /**
         * Overriding of paintComponent. Draws the game according to the
         * toString method in GameManager, with images for the walls, snake and
         * its head, food, and any easter eggs.
         * @param g the Graphics object that repaints the panel
         */
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            String board = manager.toString();
            if (board == null) {
                board = "";
            }

            String[] eachLine = board.split("\\n");
            int rows = eachLine.length;
            int cols = eachLine[0].length();

            //If Harambe easter egg is set, paints only the image of it
            //and skips the rest.
            if (wantHarambe && timeCount < 15 && timeCount > 0 &&
                    nameOfMap.equals("maze-cross.txt")) {
                g.drawImage(harambe, 0,0, gameWindow.getWidth(),
                        gameWindow.getHeight(),null);
                frame.setTitle("RIP Harambe");
            }

            //If the Harambe easter egg is not activated, then just paint
            //the panel as normal - walls, body, head (dependent on direction),
            //and food.
            else {
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < cols; j++) {
                        char character = eachLine[i].charAt(j);
                        if (character == Chars.WALL.toChar()) {
                            g.drawImage(wallImage, j * squareSize,
                                    i * squareSize, null);
                        } else if (character == Chars.HEAD.toChar()) {
                            switch (direction) {
                                case RIGHT:
                                    g.drawImage(snakeHeadRight, j * squareSize,
                                            i * squareSize, null);
                                    break;
                                case LEFT:
                                    g.drawImage(snakeHeadLeft, j * squareSize,
                                            i * squareSize, null);
                                    break;
                                case DOWN:
                                    g.drawImage(snakeHeadDown, j * squareSize,
                                            i * squareSize, null);
                                    break;
                                default:
                                    g.drawImage(snakeHeadUp, j * squareSize,
                                            i * squareSize, null);
                            }
                        } else if (character == Chars.BODY.toChar()) {
                            g.drawImage(snakeBody, j * squareSize,
                                    i * squareSize, null);
                        } else if (character == Chars.FOOD.toChar()) {
                            g.drawImage(currentFoodImage, j * squareSize,
                                    i * squareSize, null);
                        }
                    }
                }
            }
        }
    }

    /**
     * Updates the game. First gets the outcome of the game and updates as
     * necessary, with a set of two dialog messages if the game ended.
     */
    private void updateGame() {
        Outcome gameState = manager.handleMove(direction);
        switch(gameState) {
            case FOOD_EATEN:
                score.setText("Current score: " + manager.getFoodEaten());
                //Randomly selects food image to place next
                currentFoodImage = images.get(random.nextInt(images.size()));
                break;
            case OFF_EDGE:
                JOptionPane.showMessageDialog(frame, "You went off the end. " +
                                "Unlike the earth, this game is flat.",
                        "Game over", JOptionPane.INFORMATION_MESSAGE);
                break;
            case HIT_SELF:
                JOptionPane.showMessageDialog(frame,"You hit yourself. " +
                        "Eating yourself is not recommended.", "Game over",
                        JOptionPane.INFORMATION_MESSAGE);
                break;
            case HIT_WALL:
                JOptionPane.showMessageDialog(frame,"You hit the wall. " +
                        "Unfortunately, you are not a battering ram.",
                        "Game over", JOptionPane.INFORMATION_MESSAGE);
                break;
        }

        //User can close the game or start a new game with the same map.
        if (!gameState.equals(Outcome.FOOD_EATEN) && !gameState.equals(Outcome.CONTINUE)) {
            isGameOver = true;
            timer.stop();
            int selection = JOptionPane.showConfirmDialog(frame, "Play again?",
                    "Play again?", JOptionPane.YES_NO_OPTION);
            if (selection == 1) System.exit(0);
            else {
                newGame(nameOfMap);
            }
        }
    }

    /**
     * Starts a new game, either initially or after the first has been played.
     * Sets up the bookkeeping for the game as well as resizing the game if a
     * different map is called.
     * @param s the name of the maze
     */
    private void newGame(String s) {
        nameOfMap = s;
        manager = new GameManager(s);
        manager.setUpGame();
        int previousSquareSize = squareSize;
        if(s.equals("maze-zigzag-large.txt")) squareSize = 15;
        else squareSize = 20;
        startPause.setText("Start");
        isGameOver = false;
        direction = manager.getCurrentDirection();
        score.setText("Current score: " + manager.getFoodEaten());
        if (gamesPlayed > 0) {

            //If the map size requires a different sqaure size, resize images
            if (previousSquareSize - squareSize != 0) {
                imageProcessing();
            }

            //When only setPreferredSize or only setSize is called the panel
            //often doesn't resize correctly
            gameWindow.setPreferredSize(new Dimension(manager.getWidth()
                    * squareSize, manager.getHeight() * squareSize));
            gameWindow.setSize(manager.getWidth()
                    * squareSize, manager.getHeight() * squareSize);
            gameWindow.repaint();
            frame.setTitle("Steak'n'snake");
            frame.pack();
            gamesPlayed++;
        }
    }

    /**
     * Constructor for the GUI. Sets up much of the initial bookkeeping such as
     * titles, frame operations, creating a new panel for the game to display
     * on, and adding a multitude of items to the frame - panels, buttons,
     * labels, and a menu.
     * @param s the name of the maze
     */
    private SnakeGUI (String s) {
        score = new JLabel();
        newGame(s);
        gamesPlayed++;
        frame = new JFrame("Steak'n'Snake");
        frame.setResizable(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameWindow = new GamePanel();
        gameWindow.setFocusable(true);

        //Key listener ignores input that sends the snake in the opposite
        //direction of where it headed before.
        gameWindow.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                switch(KeyEvent.getKeyText(e.getKeyCode())) {
                    case "Up":
                        if (!manager.getCurrentDirection().
                                        equals(Snake.Direction.DOWN)) {
                            direction = Snake.Direction.UP;
                        }
                        break;
                    case "Down":
                        if (!manager.getCurrentDirection().
                                        equals(Snake.Direction.UP)) {
                            direction = Snake.Direction.DOWN;
                        }
                        break;
                    case "Right":
                        if (!manager.getCurrentDirection().
                                        equals(Snake.Direction.LEFT)) {
                            direction = Snake.Direction.RIGHT;
                        }
                        break;
                    case "Left":
                        if (!manager.getCurrentDirection().
                                        equals(Snake.Direction.RIGHT)) {
                            direction = Snake.Direction.LEFT;
                        }
                        break;
                }
            }
        });

        //Adds action listener to the start/pause button; stops the timer
        //and thus stops any game action from happening if the command is to
        //pause the game and gives the window focus and starts the timer again
        //if the command is to start it.
        startPause.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getActionCommand().equals("Start")) {
                    if (!isGameOver) {
                        gameWindow.requestFocusInWindow();
                        timer.start();
                    }
                } else {
                    timer.stop();
                }
                String updatedLabel = changeLabel(e.getActionCommand());
                startPause.setText(updatedLabel);
            }
        });

        JPanel bottomPanel = new JPanel(new FlowLayout());
        bottomPanel.add(startPause); bottomPanel.add(score);
        JMenuBar bar = new JMenuBar();

        //Adds a menu with options to start a new game for each map
        JMenu options = new JMenu("New Game");
        addMenuOption(options, "Simple");
        addMenuOption(options, "Cross");
        addMenuOption(options, "Stripes");
        addMenuOption(options, "Small Zigzag");
        addMenuOption(options, "Large Zigzag");
        bar.add(options);

        frame.setJMenuBar(bar);
        frame.add(bottomPanel, "South");
        frame.add(gameWindow, "Center");
        frame.pack();
        frame.setVisible(true);
        startPause.requestFocus();
    }

    /**
     * Sets a new text for the label used by the start/pause button.
     * @param s the current string displayed on the button
     * @return the opposite string; if the current string is "Pause" then it
     * returns "Start" and vice versa.
     */
    private String changeLabel(String s) {
        String r = "Start";
        if (r.equals(s)) r = "Pause";
        return r;
    }

    /**
     * Adds a menu option to the new game menu.
     * @param menu the menu being added to
     * @param itemName the name of the new menu item
     */
    private void addMenuOption(JMenu menu, String itemName) {
        JMenuItem option = new JMenuItem(itemName);
        addMenuListener(option);
        menu.add(option);
    }

    /**
     * Adds an action listener to a menu item; starts a new game with the
     * specified map if clicked.
     * @param item the menu item getting an action listener added
     */
    private void addMenuListener(JMenuItem item) {
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                switch (e.getActionCommand()) {
                    case "Simple":
                        newGame("maze-simple.txt");
                        break;
                    case "Cross":
                        newGame("maze-cross.txt");
                        break;
                    case "Stripes":
                        newGame("maze-stripes.txt");
                        break;
                    case "Small Zigzag":
                        newGame("maze-zigzag-small.txt");
                        break;
                    case "Large Zigzag":
                        newGame("maze-zigzag-large.txt");
                        break;
                }
            }
        });
    }

    /**
     * Main method to run the GUI. Runs the game on the event dispatch thread
     * via using a Runnable. Checks the command line arguments to see if any
     * activate easter eggs or are maze names.
     * @param args the command line arguments, if any.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (args.length > 0) {
                    //Easter eggs
                    for (String s: args) {
                        if (s.toUpperCase().equals("EVIL")) {
                            wantYankees = true;
                        } if (s.toUpperCase().equals("FIRE")) {
                            wantFire = true;
                        } if (s.toUpperCase().equals("HARAMBE")) {
                            wantHarambe = true;
                        } if (s.toUpperCase().equals("MINECRAFT")) {
                            wantMinecraft = true;
                        }
                    }
                    String s = args[0].toUpperCase();
                    switch (s) {
                        case "CROSS":
                            new SnakeGUI("maze-cross.txt");
                            break;
                        case "STRIPES":
                            new SnakeGUI("maze-stripes.txt");
                            break;
                        case "S-ZIGZAG":
                        case "ZIGZAG-SMALL":
                        case "Small-ZIGZAG":
                            new SnakeGUI("maze-zigzag-small.txt");
                            break;
                        case "L-ZIGZAG":
                        case "ZIGZAG-LARGE":
                        case "LARGE-ZIGZAG":
                            new SnakeGUI("maze-zigzag-large.txt");
                            break;
                        default:
                            new SnakeGUI("maze-simple.txt");
                    }
                }

                //If no arguments given, starts a normal game with the simple
                //map
                else new SnakeGUI("maze-simple.txt");
            }
        });
    }
}