//Author: Andrew Barndt
//Description: A small java class that sets up the walls for a game of snake

import java.util.ArrayList;

public class Wall {

    //Lists to hold the x and y coordinates of each part of the wall
    public ArrayList<Integer> xNumbers = new ArrayList<>();
    public ArrayList<Integer> yNumbers = new ArrayList<>();

    //Wall constructor. Takes four integer parameters: the first is the
    //start of the horizontal direction, the second is the start of the
    //vertical direction, the third is the end of the horizontal direction,
    //and the fourth is the end of the vertical direction.
    public Wall (int a, int b, int c, int d) {

        //Add every horizontal coordinate to the x collection
        for (int i = a; i <= c; i++) {
            xNumbers.add(i);
        }

        //Add every vertical coordinate to the y collection
        for (int i = b; i <= d; i++) {
            yNumbers.add(i);
        }
    }
}