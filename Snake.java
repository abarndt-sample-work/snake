//Author: Andrew Barndt
//Description: A java class that defines a Snake object.

public class Snake {

    //Defines the possible directions the snake can travel
    public enum Direction {
        UP(0,-1), DOWN(0,1), LEFT(-1,0), RIGHT(1,0), REST(0,0);

        //Defines the change each direction causes
        private int xChange, yChange;
        Direction(int x, int y) {
            this.xChange = x;
            this.yChange = y;
        }

        /**
         * Getter method for the horizontal change the direction causes
         * @return the horizontal change
         */
        public int getXChange() {
            return this.xChange;
        }

        /**
         * Getter method for the vertical change the direction causes
         * @return the vertical change
         */
        public int getYChange() {
            return this.yChange;
        }

    }

    //Declare the variable representing the snake's length
    private int length;

    /**
     * Constructor for a snake object. Initializes the snake to the initial
     * size of length 5.
     */
    public Snake() {
        this.length = 5;
    }

    /**
     * Getter method for the snake's length at that point in time.
     * @return the snake's length
     */
    public int getLength() {
        return this.length;
    }

    /**
     * Method to increase the length of the snake; called whenever the snake
     * eats food.
     */
    public void increaseLength() {
        this.length += 3;
    }
}