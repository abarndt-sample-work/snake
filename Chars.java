//Author: Andrew Barndt
//Description: A java enum that outlines the characters used to represent
//the parts of the snake game. Adapted from Square enum from Gomoku.

public enum Chars {

    HEAD('S'), BODY('s'), FOOD('f'), WALL('X'), EMPTY('.');

    private final char symbol;
    Chars(char a) { this.symbol = a; }
    public char toChar() {return this.symbol;}
}