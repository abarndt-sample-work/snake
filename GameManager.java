//Author: Andrew Barndt
//Description: a Java class to implement the game logic for Snake.
//Plays the game like a board with a 2D array; the graphical interface
//for the game will be based on that setup.

import javafx.util.Pair;
import java.io.*;
import java.util.*;

public class GameManager {

    //Global board with rows represented by height and columns represented
    //by width
    private int height, width;
    private char board[][];

    //Storing the walls in a list is the most convenient option
    private List<Wall> walls = new ArrayList<>();
    private Snake snake;

    //Use a list of pairs to store the locations at which the snake is in
    private List<Pair<Integer, Integer>> locations = new ArrayList<>();

    //Random number generator
    private Random rand = new Random();

    //Global variables useful for updating the game appropriately
    private int foodEaten;
    private int emptySpaceCounter;
    private Snake.Direction currentDirection;
    private Outcome currentState;

    //Characters to place on the board to represent gameplay
    private char snakeHead = Chars.HEAD.toChar();
    private char snakeBody = Chars.BODY.toChar();
    private char open = Chars.EMPTY.toChar();
    private char food = Chars.FOOD.toChar();
    private char wallChar = Chars.WALL.toChar();

    /**
    Constructor for the game manager. Takes a string (the name of some file),
    reads it, and uses a couple scanners to collect the information necessary
    to set up the game's size and boundaries. The first two numbers on the
    first line are for the width and height of the game; the next lines hold
    the x starting point, y starting point, x ending point, and y ending point
    of each wall, which are then all added to a collection of walls for this
    particular manager.
     */
    public GameManager(String s) {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream(s);
             Scanner scanner = new Scanner(new BufferedReader(new InputStreamReader(in)))) {
            String r = scanner.nextLine();
            Scanner smallerScale = new Scanner(r);
            this.width = smallerScale.nextInt();
            this.height = smallerScale.nextInt();
            while (scanner.hasNextLine() && scanner.hasNext()) {
                Wall w = new Wall (scanner.nextInt(), scanner.nextInt(),
                        scanner.nextInt(), scanner.nextInt());
                walls.add(w);
            }
        } catch (Exception e) {
            System.out.println("Error loading " + s);
        }
    }

    /**
     * Sets up the game in a few stages. First, it sets the game size
     * according to the information the constructor gathered, creates a snake
     * object, and then calls methods to fill the board with the components of
     * a Snake game.
     */
    public void setUpGame() {
        this.board = new char[height][width];
        this.snake = new Snake();
        currentDirection = Snake.Direction.REST;
        foodEaten = 0;
        locations.clear();
        initialFill();
        wallsToArray();
        snakeStart();
        newFood();
    }

    /**
     * Getter method for the height of the board. Will be called by the GUI.
     * @return the height of the current board.
     */
    public int getHeight() {
        return this.height;
    }

    /**
     * Getter method for the width of the board. Will also be called by the
     * GUI.
     * @return the width of the current board.
     */
    public int getWidth() {
        return this.width;
    }

    /**
     * Getter method for the amount of food eaten.
     * @return the number of pieces of food the snake in the current game has
     * eaten.
     */
    public int getFoodEaten() {
        return this.foodEaten;
    }

    /**
     * Getter method for the current direction of the snake.
     * @return the snake's current direction.
     */
    public Snake.Direction getCurrentDirection() {
        return this.currentDirection;
    }

    /**
     * Getter method for the current outcome of the game.
     * @return the current outcome of the game.
     */
    public Outcome getCurrentState() {
        return this.currentState;
    }

    /**
     * Provides a string representation of the board; will indicate where the
     * walls are, where the snake currently is, and where the food item is.
     * @return a string representation of the board, which is changed after
     * every move.
     */
    public String toString() {
        String s = "";
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                s += board[i][j];
            }
            s += "\n";
        }
        return s;
    }

    /**
     * Fills the board with all empty spaces; called before the walls, snake,
     * and food are placed on the board.
     */
    private void initialFill () {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                board[i][j] = open;
            }
        }
    }

    /**
     * Adds the walls created in the constructor to the board. Uses a for-each
     * loop to go through each wall, then loops through all height values of
     * the wall and adds a wall character to the board in the specified column.
     */
    private void wallsToArray() {
        for (Wall wall: walls) {
            for (int i = wall.yNumbers.get(0); i < wall.yNumbers.get(0) +
                    wall.yNumbers.size(); i++) {
                for (int j = wall.xNumbers.get(0); j < wall.xNumbers.get(0) +
                        wall.xNumbers.size(); j++) {
                    board[i][j] = wallChar;
                }
            }
        }
    }

    /**
     * Places a new food item on the board, both at the start of the game and
     * whenever the snake eats another piece of food. Uses the random numbers
     * created earlier to create possible coordinates for the food, then checks
     * if those coordinates are either occupied or are directly in the space
     * the snake would move into next. If it finds the location would be
     * invalid, the method calls itself and makes a new set of random
     * coordinates and tests those.
     */
    private void newFood() {
        int yLocation = rand.nextInt(height);
        int xLocation = rand.nextInt(width);
        if (board[yLocation][xLocation] == open &&
                ((locations.get(0).getKey() + currentDirection.getYChange()
                        != yLocation && locations.get(0).getValue() +
                currentDirection.getXChange() != xLocation)
                        || emptySpaceCounter == 1)) {
                board[yLocation][xLocation] = food;
        } else newFood();
    }

    /**
     * Sets up the snake's initial position. The snake is at rest by default,
     * so the snake will not move until it is commanded to by the player. It
     * then sets up random coordinates like the newFood method does, then
     * checks if those coordinates are occupied. If not, the head of the snake
     * is placed there and the coordinates are saved in a pair that gets added
     * to the list of pairs the snake occupies; if the coordinates are
     * occupied, it retries until it finds coordinates that work.
     */
    private void snakeStart() {
        this.currentDirection = Snake.Direction.REST;
        int yLocation = rand.nextInt(height);
        int xLocation = rand.nextInt(width);
        if (board[yLocation][xLocation] == open) {
            board[yLocation][xLocation] = snakeHead;
            Pair<Integer, Integer> pair = new Pair<>(yLocation, xLocation);
            locations.add(pair);
        } else snakeStart();
    }

    /**
     * Handles the move in the direction the user indicates. First checks if
     * the move is in the opposite direction from the current direction, then
     * checks if the snake is going off the edge of the game, then sees if the
     * location being moved to is occupied and adjusts the game appropriately,
     * and finally calls the method to update the board.
     * @param d the direction the user directs the snake to move.
     * @return th   e outcome of the move.
     */
    public Outcome handleMove(Snake.Direction d) {
        currentState = Outcome.CONTINUE; //Default outcome
        boolean isOpposite = false;

        //Checks if snake is being turned in opposite direction - if it is,
        //the snake will simply continue moving in the same direction it was
        //previously moving
        if ((d.equals(Snake.Direction.UP) &&
                currentDirection.equals(Snake.Direction.DOWN))) {
            currentDirection = Snake.Direction.DOWN;
            isOpposite = true;
        } else if (d.equals(Snake.Direction.DOWN) &&
                currentDirection.equals(Snake.Direction.UP)) {
            currentDirection = Snake.Direction.UP;
            isOpposite = true;
        } else if((d.equals(Snake.Direction.LEFT)) &&
                currentDirection.equals(Snake.Direction.RIGHT)) {
            currentDirection = Snake.Direction.RIGHT;
            isOpposite = true;
        } else if (d.equals(Snake.Direction.RIGHT) &&
                currentDirection.equals(Snake.Direction.LEFT)) {
            currentDirection = Snake.Direction.LEFT;
            isOpposite = true;
        }

        if(!isOpposite) currentDirection = d;

        //If the move causes the snake to go off the edge of the game, the
        //game state is changed as that will result in a loss
        if (currentDirection.equals(Snake.Direction.UP)) {
            if (locations.get(0).getKey() == 0)
                currentState = Outcome.OFF_EDGE;
        } else if (currentDirection.equals(Snake.Direction.DOWN)) {
            if (locations.get(0).getKey() == (height - 1))
                currentState = Outcome.OFF_EDGE;
        } else if (currentDirection.equals(Snake.Direction.LEFT)) {
            if (locations.get(0).getValue() == 0)
                currentState = Outcome.OFF_EDGE;
        } else if (currentDirection.equals(Snake.Direction.RIGHT)) {
            if (locations.get(0).getValue() == (width - 1)) {
                currentState = Outcome.OFF_EDGE;
            }
        }

        //Check to see if move will result in the snake hitting the wall,
        //itself, or a food location
        if (!currentState.equals(Outcome.OFF_EDGE)) {
            if (board[locations.get(0).getKey() + currentDirection.getYChange()]
                    [locations.get(0).getValue() + currentDirection.getXChange()]
                    == wallChar) {
                currentState = Outcome.HIT_WALL;
            } else if (board[locations.get(0).getKey() + currentDirection.getYChange()]
                    [locations.get(0).getValue() + currentDirection.getXChange()]
                    == snakeBody) {
                currentState = Outcome.HIT_SELF;
            } else if (board[locations.get(0).getKey() + currentDirection.getYChange()]
                    [locations.get(0).getValue() + currentDirection.getXChange()] == food) {
                currentState = Outcome.FOOD_EATEN;
                foodEaten++;
            }
        }

        //Checks for the special case where the snake's head is moving into
        //the space occupied by the snake's last piece; since that space will
        //open up, the snake is free to move there and the game continues
        if (locations.get(locations.size() - 1).getKey() ==
                locations.get(0).getKey() + currentDirection.getYChange() &&
                locations.get(locations.size() - 1).getValue() ==
                        locations.get(0).getValue() + currentDirection.getXChange()) {
            currentState = Outcome.CONTINUE;
        }

        //Calls updateBoard after all tests are run
        updateBoard(currentDirection);

        return currentState;
    }

    /**
     * Updates the board with the snake's new location(s). First adds a pair
     * of coordinates to the list of locations the snake's body is at, adjusts
     * the board with those new locations, then removes the snake's last
     * location (at the end of its tail) if the size is too large.
     * @param d the direction the snake's movement is going.
     */
    private void updateBoard(Snake.Direction d) {

        //Calculates new location by taking previous location and adding
        //whatever the change in x and y directions are
        int yInPair = locations.get(0).getKey() + d.getYChange();
        int xInPair = locations.get(0).getValue() + d.getXChange();
        Pair<Integer, Integer> pair = new Pair<>(yInPair, xInPair);
        locations.add(0, pair);
        int yCoordinate, xCoordinate;

        //Removes the last element from the snake's body if its body is too
        //large.
        if(locations.size() > snake.getLength()) {
            yCoordinate = locations.get(locations.size() - 1).getKey();
            xCoordinate = locations.get(locations.size() - 1).getValue();
            board[yCoordinate][xCoordinate] = open;
            locations.remove(locations.size() - 1);
        }

        //Puts the body where the locations collection says it should be,
        //excluding the first element in the location which is the head
        for (int i = 1; i < locations.size(); i++) {
            yCoordinate = locations.get(i).getKey();
            xCoordinate = locations.get(i).getValue();
            board[yCoordinate][xCoordinate] = snakeBody;
        }

        //If the snake's head is not running off the edge, then it is also
        //put on the board
        if (!currentState.equals(Outcome.OFF_EDGE)) {
            board[locations.get(0).getKey()][locations.get(0).getValue()]
                    = snakeHead;
        }

        //Empty space counter; not used by program until only one space
        //remains, at which point the newFood method ignores the game rule
        //about not putting food directly in front of the snake
        emptySpaceCounter = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (board[i][j] == open) emptySpaceCounter++;
            }
        }

        //If food is eaten, make the snake larger and place a new food item
        if (currentState.equals(Outcome.FOOD_EATEN)) {
            snake.increaseLength();
            newFood();
        }
    }
}